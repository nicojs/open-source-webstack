﻿using InfoSupport.KanziDesigner.BackEnd.Models;
using Kanzi.Design;
using Kanzi.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfoSupport.KanziDesigner.BackEnd.Util
{
    internal class TenantHelper
    {

        private static SurveyConstraintsModel _default;
        internal static SurveyConstraintsModel Default
        {
            get
            {
                if (_default == null)
                {
                    // As of now, copy paste from js file:
                    _default = ChooseCasingJsonFormatter.DesializeObjectCamelCase<SurveyConstraintsModel>(
                    #region json
@"    {
        questionVisualStyles: [
          { name: 'SmileyFaces', questionType: { name: 'Single', availableValidators: 'Required' } },
          { name: 'LargeText', questionType: { name: 'Text', availableValidators: 'Required, Numeric' } },
          { name: 'SmallText', questionType: { name: 'Text', availableValidators: 'Required, Numeric' } },
          { name: 'Date', questionType: { name: 'Text', availableValidators: 'Required, Numeric' } }
        ],
        scoreCategories: [{ name: 'Points' }],
        tagCategories: [{ key: 'Region' }, { key: 'SectionType' }],
        conclusionRuleSets: [{
          name: 'All',
          availableProperties: [{ name: 'TEXT' }],
          rules: []
        }, {
          name: 'Products',
          availableProperties: [{ name: 'TEXT' }],
          rules: []
        }]
     }"
                    #endregion
);
                }
                return _default;
            }
        }

        public static DesignerContext GetDesignValidatorContext()
        {
            var context = new DesignerContext();
            foreach (var questionType in Default.QuestionVisualStyles.Select(style => style.QuestionType.Name).Distinct())
            {
                context.RegisterQuestionType(questionType);
            }
            foreach (var visualStyle in Default.QuestionVisualStyles)
            {
                context.RegisterStyle(visualStyle.Name, visualStyle.QuestionType.Name);
            }
            foreach (var questionType in Default.QuestionVisualStyles.Select(visualStyle => visualStyle.QuestionType).Distinct(SurveyConstraintsModel.QuestionTypeEqualityComparer.Instance))
            {
                if (questionType.AvailableValidators != null)
                {
                    foreach (var validator in questionType.AvailableValidators.Split(',').Select(validatorName => validatorName.Trim()))
                    {
                        context.RegisterValidator(questionType.Name, new Kanzi.Design.Validation.ValidatorMetadata
                            {
                                Name = validator,
                            });
                    }
                }
            }
            return context;
        }

    }
}