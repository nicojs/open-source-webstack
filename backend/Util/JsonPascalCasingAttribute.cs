﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfoSupport.KanziDesigner.BackEnd.Util
{
    [AttributeUsage(AttributeTargets.Class)]
    public class JsonDefaultCasingAttribute : Attribute
    {

    }
}