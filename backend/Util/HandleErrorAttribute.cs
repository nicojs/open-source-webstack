﻿using Kanzi.Design;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;

namespace InfoSupport.KanziDesigner.BackEnd.Util
{
    public class HandleErrorAttribute : ExceptionFilterAttribute, IExceptionFilter
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            var parseError = context.Exception as ParseException;
            if (parseError != null)
            {
                context.Response = context.Request.CreateErrorResponse(HttpStatusCode.BadRequest, parseError.Message);
            }
            base.OnException(context);
        }
    }
}