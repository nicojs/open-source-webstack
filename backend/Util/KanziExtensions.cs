﻿using Kanzi.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace InfoSupport.KanziDesigner.BackEnd.Util
{
    internal static class KanziExtensions
    {
        internal static void SetDefaultInfo(this Survey survey)
        {
            survey.Info.MajorVersion = int.Parse(DateTime.Now.ToString("yyyyMMdd", CultureInfo.InvariantCulture), CultureInfo.InvariantCulture);
            survey.Info.MinorVersion = int.Parse(DateTime.Now.ToString("HHmmssfff", CultureInfo.InvariantCulture), CultureInfo.InvariantCulture);
            survey.Info.TenantId = KanziConstants.TenantId;
        }
    }
}