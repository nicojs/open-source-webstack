﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Routing;

namespace InfoSupport.KanziDesigner.BackEnd.Util
{
    public class ChooseCasingJsonFormatter : JsonMediaTypeFormatter
    {
        private IHttpRouteData _route;

        public ChooseCasingJsonFormatter()
        {
            SerializerSettings.TypeNameHandling = TypeNameHandling.Auto;
        }

        private static Lazy<string[]> JsonDefaultCasingControllerNames = new Lazy<string[]>( () =>
            {
                List<string> defaultCasingControllerNames = new List<string>();
                foreach(var controllerType in Assembly.GetCallingAssembly().GetTypes().Where( type => type.Name.EndsWith("Controller")))
                {
                    if(controllerType.GetCustomAttribute(typeof(JsonDefaultCasingAttribute)) != null)
                    {
                        string shortName = controllerType.Name.Split('.').Last();
                        // strip 'Controller'
                        defaultCasingControllerNames.Add(shortName.Substring(0, shortName.Length-10));
                    }
                }
                return defaultCasingControllerNames.ToArray();
            }, true);

    

        private static CamelCasePropertyNamesContractResolver _CamelCase = new CamelCasePropertyNamesContractResolver();
        private static DefaultContractResolver _DefaultCase = new DefaultContractResolver();

        public override MediaTypeFormatter GetPerRequestFormatterInstance(Type type, HttpRequestMessage request, System.Net.Http.Headers.MediaTypeHeaderValue mediaType)
        {
            _route = request.GetRouteData();
            return base.GetPerRequestFormatterInstance(type, request, mediaType);
        }

        public override Task WriteToStreamAsync(Type type, object value, Stream writeStream, HttpContent content, TransportContext transportContext)
        {
            return Task.Factory.StartNew(() =>
            {
                SerializerSettings.ContractResolver = _CamelCase;
                foreach(var controllerName in JsonDefaultCasingControllerNames.Value)
                {
                    if (string.Equals(controllerName, _route.Values["controller"].ToString(), StringComparison.OrdinalIgnoreCase))
                    {
                        SerializerSettings.ContractResolver = _DefaultCase;
                    }
                }
                base.WriteToStreamAsync(type, value, writeStream, content, transportContext).Wait();
            });
        }

        private static JsonSerializerSettings GetCamelCaseSerializerSettings()
        {
            return new JsonSerializerSettings
            {
                ContractResolver =  _CamelCase,
                TypeNameHandling = TypeNameHandling.Auto,
            };
        }

        internal static T DesializeObjectCamelCase<T>(string objectJson)
        {
            var jsonObject = JsonConvert.DeserializeObject<T>(objectJson,
                GetCamelCaseSerializerSettings());
            return jsonObject;
        }
    }
}