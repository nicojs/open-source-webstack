﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfoSupport.KanziDesigner.BackEnd.Models
{
    public class SurveyConstraintsModel
    {
        public class QuestionVisualStyle
        {
            public string Name { get; set; }
            public QuestionType QuestionType { get; set; }
        }

        public class QuestionType
        {
            public string Name { get; set; }
            public string AvailableValidators { get; set; }
        }

        public class QuestionTypeEqualityComparer : IEqualityComparer<QuestionType>
        {
            private QuestionTypeEqualityComparer()
            { }

            private static QuestionTypeEqualityComparer _instance;
            public static QuestionTypeEqualityComparer Instance
            {
                get
                {
                    if (_instance == null)
                    {
                        _instance = new QuestionTypeEqualityComparer();
                    }
                    return _instance;
                }
            }

            public bool Equals(QuestionType x, QuestionType y)
            {
                return string.Equals(x.Name, y.Name, StringComparison.InvariantCulture);
            }

            public int GetHashCode(QuestionType obj)
            {
                return obj.Name.GetHashCode();
            }
        }

        public class ScoreCategory
        {
            public string Name { get; set; }
        }

        public class TagCategory
        {
            public string Key { get; set; }
        }

        public class ConclusionRuleProperty
        {
            public string Name { get; set; }
        }

        public class ConclusionRuleSet
        {
            public string Name { get; set; }
            public ConclusionRuleProperty[] AvailableProperties { get; set; }
        }

        public QuestionVisualStyle[] QuestionVisualStyles { get; set; }
        public ScoreCategory[] ScoreCategories { get; set; }
        public TagCategory[] TagCategories { get; set; }
        public ConclusionRuleSet[] ConclusionRuleSets { get; set; }
    }
}