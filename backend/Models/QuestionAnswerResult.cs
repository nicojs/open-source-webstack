﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfoSupport.KanziDesigner.BackEnd.Models
{
    public class QuestionAnswerResult
    {
        public string QuestionText { get; set; }
        public string AnswerText { get; set; }
    }
}