﻿using Kanzi.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfoSupport.KanziDesigner.BackEnd.Models
{
    public class SurveySummariesPage
    {

        public int TotalPageCount { get; set; }
        public int CurrentPageIndex { get; set; }

        public List<SurveySummary> Items { get; set; }
    }
}