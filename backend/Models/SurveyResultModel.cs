﻿using Kanzi.Model;
using Kanzi.Model.Conclusions;
using Kanzi.Model.Facts;
using Kanzi.Model.Questionnaires;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfoSupport.KanziDesigner.BackEnd.Models
{
    public class SurveyResultModel
    {
        public SurveyResultModel(ISurveyState surveyState, Survey survey, SurveyConstraintsModel tenantConstraints)
        {
            var result = survey.GetResult(surveyState.GivenAnswers);
            ConclusionSets = result.ConclusionSets;
            Facts = result.Facts;

            // Retrieve scores
            Scores = new List<ScoreResult>();
            foreach (var scoreCategory in tenantConstraints.ScoreCategories)
            {
                Scores.Add(new ScoreResult { CategoryName = scoreCategory.Name, Score = result.ScoreCalculator.GetTotalScore(scoreCategory.Name) });
            }

            // Retrieve questions and answers
            QuestionAnswerResults = ToQuestionAnswerResults(surveyState, survey);
        }

        private static List<QuestionAnswerResult> ToQuestionAnswerResults(ISurveyState surveyState, Survey survey)
        {
            var questionAnswerResults = new List<QuestionAnswerResult>();
            foreach (var givenAnswer in surveyState.GivenAnswers)
            {
                string answerText = null;
                if (givenAnswer.Fields != null)
                {
                    var answerField = givenAnswer.Fields.FirstOrDefault(field => field.Name == "Text");
                    if (answerField != null)
                    {
                        answerText = answerField.Value;
                    }
                }
                string questionText = null;
                if (survey.Questionnaire.Pages != null)
                {
                    foreach (var page in survey.Questionnaire.Pages)
                    {
                        if (page.Questions != null)
                        {
                            foreach (var question in page.Questions)
                            {
                                if (question.Id == givenAnswer.QuestionId)
                                {
                                    questionText = question.Text;
                                    // Als fill answer text if multiple choice question
                                    if (answerText == null)
                                    {
                                        var answer = question.Answers.FirstOrDefault(ansr => ansr.Id == givenAnswer.AnswerId);
                                        if (answer != null)
                                        {
                                            answerText = answer.Text;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(questionText))
                        {
                            break;
                        }
                    }
                }
                if (!string.IsNullOrEmpty(questionText))
                {
                    questionAnswerResults.Add( new QuestionAnswerResult { AnswerText = answerText, QuestionText = questionText, });
                }
            }
            return questionAnswerResults;
        }

        public List<ScoreResult> Scores { get; set; }
        public ConclusionSetCollection ConclusionSets { get; set; }
        public FactCollection Facts { get; set; }
        public List<QuestionAnswerResult> QuestionAnswerResults { get; set; }
    }
}