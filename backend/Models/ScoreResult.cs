﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfoSupport.KanziDesigner.BackEnd.Models
{
    public class ScoreResult
    {
        public int? Score { get; set; }
        public string CategoryName { get; set; }
    }
}