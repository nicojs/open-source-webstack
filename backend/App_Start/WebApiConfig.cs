﻿using InfoSupport.KanziDesigner.BackEnd.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace InfoSupport.KanziDesigner.BackEnd
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Json settings
            config.Formatters.RemoveAt(0);
            config.Formatters.Insert(0, new ChooseCasingJsonFormatter());

            config.IncludeErrorDetailPolicy
                = IncludeErrorDetailPolicy.Always;

            config.Filters.Add(new HandleErrorAttribute());

            // Enable Cross Origin Requests (CORS) globaly
            // For more information see http://msdn.microsoft.com/en-us/magazine/dn532203.aspx
            config.EnableCors(new EnableCorsAttribute("*", "*", "*"));
        }

    }
}
