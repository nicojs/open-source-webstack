﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kanzi.Design.Conditions;
using Kanzi.Model.Conditions;
using Newtonsoft.Json;
using InfoSupport.KanziDesigner.BackEnd.Util;

namespace InfoSupport.KanziDesigner.BackEnd.Controllers
{
    public class ConditionController : KanziApiController
    {
        // GET api/Condition/?text=Add....
        public Condition GetCondition(string text)
        {
            return GetCondition(text, true);
        }

        // GET api/Condition/?text=Add....&allowAdvancedConstructs=false
        public Condition GetCondition(string text, bool allowAdvancedConstructs)
        {
            return new ConditionParser { AllowAdvancedConstructs = allowAdvancedConstructs }.ParseCondition(text);
        }


        public string GetString(string conditionJson)
        {
            var condition = ChooseCasingJsonFormatter.DesializeObjectCamelCase<Condition>(conditionJson);
            return condition.ToString(false);
        }
        
    }
}