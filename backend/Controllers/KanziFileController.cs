﻿using System.Configuration;
using System.Data.SqlClient;
using System.Runtime.InteropServices.ComTypes;
using System.Threading;
using System.Web.UI.WebControls.WebParts;
using Kanzi.Model;
using Kanzi.Repository;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Kanzi.Model.Conclusions;
using Kanzi.Model.Conditions;
using Kanzi.Design.Conditions;
using System.Net.Http.Headers;
using System.IO;
using InfoSupport.KanziDesigner.BackEnd.Util;
using System.Threading.Tasks;

namespace InfoSupport.KanziDesigner.BackEnd.Controllers
{
    public class KanziFileController : KanziApiController
    {

        public HttpResponseMessage GetBySurveyName(string surveyName)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var survey = KanziEngine.GetSurvey(surveyName, KanziConstants.TenantId);
            result.Content = new StreamContent(GenerateStreamFromString(survey.ToXml()));
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            result.Content.Headers.Add("Content-Disposition", string.Format(CultureInfo.InvariantCulture, "attachment;filename={0}", GetFileName(survey)));
            return result;
        }

        private static string GetFileName(Survey survey)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0}_{1}_{2}.kanzi", WebUtility.UrlEncode(survey.Info.Name), survey.Info.MajorVersion, survey.Info.MinorVersion);
        }

        private static Stream GenerateStreamFromString(string stringToRead)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(stringToRead);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        const string XML_START = @"<?xml version=""1.0"" encoding=""utf-8""?>";
        const string XML_END = "</Survey>";
        public async Task<HttpResponseMessage> Post()
        {
            // When changing this code be sure to test it in IE8, as this was the only way i could get it to work
            var contentString = await Request.Content.ReadAsStringAsync();
            int startXmlIndex = contentString.IndexOf(XML_START);
            int endXmlIndex = contentString.IndexOf(XML_END);
            var kanziFileContent = contentString.Substring(startXmlIndex, endXmlIndex - startXmlIndex + XML_END.Length);
            var survey = Survey.FromXml(kanziFileContent);
            if (ValidateSurvey(survey))
            {
                survey.SetDefaultInfo();
                KanziEngine.PublishSurvey(survey, KanziConstants.TenantId);
                return Request.CreateResponse(survey);
            }
            else
            {
                // Cannot create an error response <> 200, because IE8 uploads using flash and error responses cannot be read via flash.
                // Just return a 200 response with the error modalstate 
                return Request.CreateErrorResponse(HttpStatusCode.OK, ModelState);
            }
        }
    }
}