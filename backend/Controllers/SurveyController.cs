﻿using System.Configuration;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using InfoSupport.KanziDesigner.BackEnd.Util;
using Kanzi.Model;

namespace InfoSupport.KanziDesigner.BackEnd.Controllers
{
    public class SurveyController : KanziApiController
    {

        // GET api/Survey/?name=CarIn
        public Survey GetByName(string name)
        {
            var survey = KanziEngine.GetSurvey(name, KanziConstants.TenantId);
            return survey;
        }

        // GET api/<controller>/5
        public Survey GetById(int id, string tenantId)
        {
            return KanziEngine.GetSurvey(id, tenantId);
        }


        // POST api/<controller>
        public HttpResponseMessage Post([FromBody]Survey survey)
        {
            if (ValidateSurvey(survey))
            {
                survey.SetDefaultInfo();
                KanziEngine.PublishSurvey(survey, KanziConstants.TenantId);
                return Request.CreateResponse(survey);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ModelState);
            }
        }

        [HttpDelete]
        public void DeleteAll()
        {
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["KanziRepository"].ConnectionString))
            using (var command = connection.CreateCommand())
            {
                connection.Open();
                command.CommandText = "DELETE FROM dbo.SurveyStates; DELETE FROM dbo.Surveys;";
                command.ExecuteNonQuery();
            }
        }
    }
}