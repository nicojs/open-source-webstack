﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kanzi.Design.Conditions;
using Kanzi.Model.Conditions;
using Newtonsoft.Json;
using InfoSupport.KanziDesigner.BackEnd.Util;
using Kanzi.Model;
using Kanzi.Repository.DataAccess;

namespace InfoSupport.KanziDesigner.BackEnd.Controllers
{
    public class SurveyStateController : KanziApiController
    {

        public ISurveyState Get(int id)
        {
            return KanziEngine.GetSurveyState(id, KanziConstants.TenantId);
        }
        
    }
}