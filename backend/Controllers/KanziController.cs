﻿using InfoSupport.KanziDesigner.BackEnd.Util;
using Kanzi.Design;
using Kanzi.Design.Extensions;
using Kanzi.Model;
using Kanzi.Model.Conclusions;
using Kanzi.Model.Questionnaires;
using Kanzi.Repository;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace InfoSupport.KanziDesigner.BackEnd.Controllers
{
    public abstract class KanziApiController : ApiController
    {
        protected RepositoryController KanziEngine { get; private set; }
        
        public KanziApiController()
        {
            KanziEngine = new RepositoryController();
        }

        protected bool ValidateSurvey(Survey survey)
        {
            ValidateSurveyExistance(survey);
            ValidateSurveyAgainstTenantSettings(survey);
            return ModelState.IsValid;
        }

        private void ValidateSurveyAgainstTenantSettings(Survey survey)
        {
            var designerContext = TenantHelper.GetDesignValidatorContext();
            foreach (var validationError in survey.Verify(designerContext))
            {
                ModelState.AddModelError(GetErrorKey(validationError), validationError.Message);
            }
        }

        private void ValidateSurveyExistance(Survey survey)
        {
            // If a survey with that name already exists and has a higher version ==> do not save
            Survey existingSurvey = null;
            try
            {
                existingSurvey = KanziEngine.GetSurvey(survey.Info.Name, KanziConstants.TenantId);
            }
            catch (System.ArgumentException)
            {
            }

            if (existingSurvey != null && (existingSurvey.Info.MajorVersion > survey.Info.MajorVersion
                || (existingSurvey.Info.MajorVersion == survey.Info.MajorVersion && existingSurvey.Info.MinorVersion > survey.Info.MinorVersion)))
            {
                ModelState.AddModelError(GENERAL_ERROR_CATEGORY,
                    string.Format(CultureInfo.InvariantCulture, "Questionnaire met naam '{0}' bestaat al of is reeds door iemand anders gewijzigd. Open de questionnaire opnieuw en probeer het nog een keer.", survey.Info.Name));
            }
        }

        private static string GetErrorKey(VerificationError validationError)
        {
            Question question = validationError.Item as Question;
            if (question != null)
            {
                return String.Format(CultureInfo.InvariantCulture, "Vraag: \"{0}\"", question.Text);
            }
            QuestionnaireElement element = validationError.Item as QuestionnaireElement;
            if (element != null)
            {
                var typeName = validationError.GetType().Name;
                return String.Format(CultureInfo.InvariantCulture, "{0}: \"{1}\"", typeName, element.Text ?? element.Alias ?? element.Id);
            }
            ConclusionRule rule = validationError.Item as ConclusionRule;
            if (rule != null)
            {
                return String.Format(CultureInfo.InvariantCulture, "Conclusie: \"{0}\"", rule.Condition.ToString(false));
            }
            return GENERAL_ERROR_CATEGORY;
        }

        const string GENERAL_ERROR_CATEGORY = "Algemeen";
    }
}