﻿using InfoSupport.KanziDesigner.BackEnd.Models;
using Kanzi.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;

namespace InfoSupport.KanziDesigner.BackEnd.Controllers
{
    public class SurveySummariesPageController : KanziApiController
    {
        private const int PAGE_SIZE = 20;

        public SurveySummariesPage Get(int id)
        {
            var page = new SurveySummariesPage();
            page.Items = new List<SurveySummary>(KanziEngine.GetLatestSurveySummaries(KanziConstants.TenantId, id * PAGE_SIZE, PAGE_SIZE));
            page.TotalPageCount = TotalNumberOfPages;
            page.CurrentPageIndex = id;
            return page;
        }

        private int TotalNumberOfPages
        {
            get
            {
                var count = KanziEngine.CountLatestSurveySummaries(KanziConstants.TenantId);
                return count / PAGE_SIZE + (count % PAGE_SIZE == 0 ? 0 : 1);
            }
        }
    }
}