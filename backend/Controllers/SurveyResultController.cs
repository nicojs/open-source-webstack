﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kanzi.Design.Conditions;
using Kanzi.Model.Conditions;
using Newtonsoft.Json;
using InfoSupport.KanziDesigner.BackEnd.Util;
using InfoSupport.KanziDesigner.BackEnd.Models;

namespace InfoSupport.KanziDesigner.BackEnd.Controllers
{
    public class SurveyResultController : KanziApiController
    {

        public SurveyResultModel GetBySurveyStateId(int id)
        {
            var surveyState = KanziEngine.GetSurveyState(id, KanziConstants.TenantId);
            var survey = KanziEngine.GetSurvey(surveyState.SurveyId, KanziConstants.TenantId);
            return new SurveyResultModel(surveyState, survey, TenantHelper.Default);
        }
    }
}