﻿using InfoSupport.KanziDesigner.BackEnd.Models;
using InfoSupport.KanziDesigner.BackEnd.Util;
using Kanzi.Commands;
using Kanzi.Dto;
using Kanzi.Model.Questionnaires;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace InfoSupport.KanziDesigner.BackEnd.Controllers
{
    [JsonDefaultCasing]
    public class PageDtoController : KanziApiController
    {

        [HttpPost]
        public HttpResponseMessage StartBySurvey([FromBody]StartSurveyInfo startInformation)
        {
            if (String.IsNullOrEmpty(startInformation.SurveyName))
            {
                startInformation.SurveyName = "ConfirmatieChecklistFocusklanten";
            }
            PageDto page = KanziEngine.StartSurvey(startInformation.SurveyName, _ => new GivenAnswerCollection(), KanziConstants.TenantId);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, page);
            return response;
        }


        // GET /api/InfoSupport.knowNow/pageDto/1?pageIndex=1 
        public HttpResponseMessage Get(int id, int pageIndex)
        {
            try
            {
                var pageDto = KanziEngine.GetPage(id, pageIndex, KanziConstants.TenantId);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, pageDto);
                return response;
            }
            catch (QuestionnaireValidationException ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.ValidationErrors);
            }
        }

        // PUT /api/InfoSupport.knowNow/pageDto/1
        public HttpResponseMessage Put(int id, SubmitPageCommand submitPageCommand)
        {
            if (submitPageCommand == null)
            {
                throw new ArgumentNullException("submitPageCommand");
            }

            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, KanziEngine.SubmitPage(id, submitPageCommand.PageIndex, submitPageCommand.GivenAnswers, KanziConstants.TenantId));
            }
            catch (QuestionnaireValidationException ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.ValidationErrors);
            }
        }
    }
}