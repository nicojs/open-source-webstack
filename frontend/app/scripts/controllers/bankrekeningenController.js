angular.module('frontendApp')
  .controller('bankrekeningenController', function ($scope, bankrekeningStorage) {
    'use strict';
    $scope.bankrekeningen = bankrekeningStorage.list();

    $scope.add = function () {
      if ($scope.newBankrekeningForm.$valid) {
        $scope.bankrekeningen.push(bankrekeningStorage.save($scope.newBankrekeningName));
      }
    };
  });
