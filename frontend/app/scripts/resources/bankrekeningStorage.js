angular.module('frontendApp')
  .factory('bankrekeningStorage', function () {
    'use strict';
    return {

      save: function(accountName){
        return {
          name: accountName,
          number: '782 2345 12',
          amount: 0
        };
      },

      list: function () {
        return [
          {
            name: 'GIT Open source',
            number: '751 234 12',
            amount: 700230
          },
          {
            name: 'Info Support bv',
            number: '515 123 12',
            amount: 10000
          }
        ];
      }
    };
  });
