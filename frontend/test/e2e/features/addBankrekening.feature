Feature: Add bankrekening
  Als client van deze bank zou ik graag een bankrekening willen toevoegen
  zodat ik mijn bankzaken goed kan regelen.

  Background:
    Given I navigate to my accounts

  Scenario: Add bankrekening succeeds
    Given I enter a new account with name "nieuwe rekening"
    When I save the new account
    Then the last account in the list should be "nieuwe rekening"

