module.exports = function () {

  var BankrekeningenPage = new require('../pages/BankrekeningenPage'),
    bankrekeningenPage = new BankrekeningenPage();

  this.Given(/^I navigate to my accounts$/, function (callback) {
    bankrekeningenPage.get();
    callback();
  });

  this.Given(/^I enter a new account with name "([^"]*)"$/, function (accountName, callback) {
    bankrekeningenPage.enterNewBankrekening(accountName);
    callback();
  });

  this.When(/^I save the new account$/, function (callback) {
    bankrekeningenPage.saveNewBankrekening();
    callback();
  });

  this.Then(/^the last account in the list should be "([^"]*)"$/, function (expectedBankrekeningName, callback) {
    var allBankrekeningNamesPromise = bankrekeningenPage.rekeningNames();
    allBankrekeningNamesPromise.then(function(actualBankrekeningNames){
      var actualBankrekeningNameElement = actualBankrekeningNames[actualBankrekeningNames.length-1];
      actualBankrekeningNameElement.getText().then(function(actualBankrekeningName){
        if (actualBankrekeningName !== expectedBankrekeningName){
          callback.fail(new Error('Expected ' + actualBankrekeningName +
            ' but was ' + expectedBankrekeningName + '.'));
        } else {
          callback();
        }
      });
    });
  });
};