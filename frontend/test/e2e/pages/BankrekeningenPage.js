module.exports = function () {

  // Go to the bankrekeningen page
  this.get = function () {
    browser.get('#/bankrekeningen');
  };


  // Page elements
  this.newBankrekeningNameInput = element(by.model('newBankrekeningName'));
  this.newBankrekeningSubmit = element(by.buttonText('Opslaan'));
  this.rows = by.repeater('bankrekening in bankrekeningen');


  this.enterNewBankrekening = function (bankrekeningName) {
    this.newBankrekeningNameInput.sendKeys(bankrekeningName);
  };

  this.saveNewBankrekening = function(){
    this.newBankrekeningSubmit.click();
  };

  this.rekeningNames = function(){
    return element.all(this.rows.column('{{bankrekening.name}}'));
  };
};