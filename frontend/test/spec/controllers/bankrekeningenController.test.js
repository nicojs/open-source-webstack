
describe('bankrekeningenController', function () {
  'use strict';
  var $scope;

  beforeEach(module('frontendApp'));

  beforeEach(inject(function ($controller, $rootScope) {
    $scope = $rootScope.$new();
    $controller('bankrekeningenController', {$scope: $scope});
  }));

  it('should list 2 bankrekeningen', function () {
    expect($scope.bankrekeningen.length).toBe(2);
  });
});